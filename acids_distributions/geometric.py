from typing import Union, Dict

import torch
from torch.nn.functional import binary_cross_entropy_with_logits

from .base import Distribution
from .bernoulli import probs_to_logits, logits_to_probs

__all__ = ['Geometric']


class Geometric(Distribution):
    def __init__(self,
                 probs: Union[torch.Tensor,
                              Dict[str, torch.Tensor], None] = None,
                 logits: Union[torch.Tensor, None] = None,
                 ) -> None:
        if isinstance(probs, dict):
            probs = probs['probs']
        self.logits = torch.tensor(0)
        self.probs = torch.tensor(0)
        self._param = torch.tensor(0)
        self._batch_shape = torch.Size([0])
        self._event_shape = torch.Size([0])
        if (probs is not None and logits is not None):
            raise AttributeError(
                'Either `probs` or `logits` should be specified, not both.')
        if (probs is None and (not isinstance(probs, torch.fx.Proxy))):
            assert (logits is not None)
            self.logits = logits
            self.probs = logits_to_probs(logits)
            self._params = logits
            self._batch_shape = logits.shape
        elif (logits is None and (not isinstance(logits, torch.fx.Proxy))):
            assert (probs is not None)
            self.probs = probs
            self.logits = probs_to_logits(probs, True, 1e-7)
            self._params = probs
            if not isinstance(probs, torch.fx.Proxy):
                assert not (probs < 0.).any(), "found probs < 0."
                assert not (probs > 1.).any(), "found probs > 1."
            self._batch_shape = logits.shape

    def __iter__(self):
        yield ('probs', self.probs)

    @property
    def mean(self):
        return 1. / self.probs - 1.

    @property
    def mode(self):
        return torch.zeros_like(self.probs)

    @property
    def variance(self):
        return (1. / self.probs - 1.) / self.probs

    @torch.no_grad()
    def sample(self):
        u = torch.rand_like(self.probs)
        return torch.floor((torch.log(u)/torch.log1p(-self.probs)))

    def log_prob(self, value):
        probs = torch.clone(self.probs)
        probs = torch.where(((probs == 1) & (value == 0)), 0, probs)
        return value * torch.log1p(-probs) + torch.log(self.probs)

    def entropy(self):
        return binary_cross_entropy_with_logits(self.logits, self.probs, reduction='none')
