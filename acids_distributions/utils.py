import torch
import torch.distributions as tdist
from . import Distribution
from .bernoulli import Bernoulli
from .categorical import Categorical
from .uniform import Uniform
from .normal import Normal


def convert_to_torch(distribution):
    if isinstance(distribution, Bernoulli):
        return tdist.Bernoulli(distribution.probs)
    elif isinstance(distribution, Categorical):
        return tdist.Categorical(logits=distribution.logits)
    if isinstance(distribution, Normal):
        return tdist.Normal(distribution.mean, distribution.stddev)
    else:
        raise NotImplementedError


class _ActDivTracer(torch.fx.Tracer):
    _dist_count_hash = {}

    def __init__(self, *args, func="forward", **kwargs):
        super(_ActDivTracer, self).__init__(*args, **kwargs)
        self.traced_func_name = func 

    def get_dist_count(self, dist_name: str):
        if not dist_name in self._dist_count_hash:
            self._dist_count_hash[dist_name] = 0
        dist_count = int(self._dist_count_hash[dist_name])
        self._dist_count_hash[dist_name] += 1
        return dist_count

    def create_arg(self, a):
        if isinstance(a, Distribution):
            dist_name = type(a).__name__
            a = self.create_proxy("call_function", type(a), args=tuple(), kwargs=dict(a), name=f"dist_{dist_name}_{self.get_dist_count(dist_name)}")
        return super(_ActDivTracer, self).create_arg(a)
