# acids_distributions

A utility repository for scriptable / graphable `torch` distributions.

## Installation
After activating the target environment, just do :
```shell
pip install -r requirements.txt
python setup.py install
```
or install it with the `pip install git+http://github.com/acids_ircam/acids_distributions`

## Current development stage

Distribution | Scriptable | Graphable
 ---: | :---: | :--- 
ExponentialFamily | X | X
Bernoulli | √ | √
Beta | X | X
Binomial | X | X
Categorical | √ | X
Cauchy | X | X
Chi2 | X | X
ContinuousBernoulli | X | X
Dirichlet | X | X
Exponential | X | X
FisherSnedecor | X | X
Gamma | X | X
Geometric | X | X
Gumbel | X | X
HalfCauchy | X | X
HalfNormal | X | X
Independent | X | X
Kumaraswamy | X | X
LKJCholesky | X | X
Laplace | X | X
LogNormal | X | X
LowRankMultivariateNormal | X | X
MixtureSameFamily | X | X
Multinomial | X | X
MultivariateNormal | X | X
NegativeBinomial | X | X
Normal | √ | √
OneHotCategorical | X | X
Pareto | X | X
Poisson | X | X
RelaxedBernoulli | X | X
LogitRelaxedBernoulli | X | X
RelaxedOneHotCategorical | X | X
StudentT | X | X
TransformedDistribution | X | X
Uniform | X | X
VonMises | X | X
Weibull | X | X
Wishart | X | X
